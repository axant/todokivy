# run on desktop
```bash
python main.py -m screen:droid2
```

the ``-m screen:droid2`` is optional

# run on andorid
```bash
buildozer --profile castix android debug run logcat
```

If the app isn't installed or launched do instead

```bash
buildozer --profile castix android debug run && adb install bin/kivytodo__armeabi-v7a-0.1-armeabi-v7a-debug.apk && buildozer android logcat
```

the ``--profile castix`` is because I wanted .buildozer directory shared between projects, you can remove it or add a new profile in buildozer.spec
