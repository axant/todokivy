from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.scrollview import ScrollView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.stacklayout import StackLayout
from kivy.uix.checkbox import CheckBox
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput
from kivy.logger import Logger
from os import path
from datetime import datetime
from functools import partial
import sqlite3


app_path = path.dirname(path.abspath(__file__))
db = sqlite3.connect(path.join(app_path, 'todo.db'))
# dict instead of numeric indexes
db.row_factory = sqlite3.Row

db.execute('''CREATE TABLE IF NOT EXISTS entries (
id INTEGER PRIMARY KEY AUTOINCREMENT,
entry_text TEXT NOT NULL,
check_date TEXT
);''')
# INSERT INTO entries (entry_text) VALUES ('display entries');


class AddScreen(Screen):
    def save_and_back(self, instance):
        entry_text = instance.parent.children[1].text
        db.execute(f'''INSERT INTO entries (entry_text) VALUES ('{entry_text}');''')
        db.commit()
        # self.manager.switch_to(self.manager.get_screen('Todo List'))
        # self.manager.get_screen('Todo List').entries_builder()
        self.manager.switch_to(app.home_screen)
        app.home_screen.entries_builder()

    def build(self):
        add_screen_root = BoxLayout(orientation='vertical')
        add_screen_root.add_widget(TextInput(id='input_entry_text'))
        add_screen_root.add_widget(Button(
            text='Save',
            on_press=lambda instance: self.save_and_back(instance)
        ))
        self.add_widget(add_screen_root)


class ListScreen(Screen):
    def entries_builder(self):
        entries = db.execute('''SELECT * FROM entries''').fetchall()
        self.entries.clear_widgets()
        for e in entries:
            print(e['entry_text'])
            entry = BoxLayout(
                orientation='horizontal',
                size_hint=(1, None),
            )
            on_press = partial(self.toggle_entry, e)
            entry.add_widget(Label(text=e['entry_text'], size_hint=(0.9, None)))
            entry.add_widget(CheckBox(
                active=e['check_date'],
                size_hint=(0.1, None),
                on_press=on_press,
            ))
            self.entries.add_widget(entry)

    def toggle_entry(self, entry, instance):
        new_state = 'NULL' if entry['check_date'] else f"'{datetime.utcnow().isoformat()}'"
        query = f'''UPDATE entries
SET check_date = {new_state}
WHERE id = {entry['id']};'''
        db.execute(query)
        db.commit()

    def build(self):
        self.root = BoxLayout(orientation='vertical')
        scrollview = ScrollView(do_scroll_y=True, do_scroll_x=True)
        self.entries = StackLayout(size_hint=(1, 0.9))
        scrollview.add_widget(self.entries)
        self.root.add_widget(scrollview)
        self.root.add_widget(Button(
            text='Add',
            size_hint=(1, None),
            # on_press=lambda instance: self.manager.switch_to(self.manager.get_screen('Add Entry')),
            on_press=lambda instance: self.manager.switch_to(app.add_screen),
        ))
        self.add_widget(self.root)
        self.entries_builder()


class TodoApp(App):
    def build(self):
        global app
        app = self

        self.sm = ScreenManager()

        self.home_screen = ListScreen(name='Todo List')
        self.add_screen = AddScreen(name='Add Entry')

        self.sm.add_widget(self.home_screen)
        self.sm.add_widget(self.add_screen)
        self.home_screen.build()
        self.add_screen.build()

        return self.sm


if __name__ == '__main__':
    TodoApp().run()
